module com.mycompany.garage {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.mycompany.garage to javafx.fxml;
    exports com.mycompany.garage;
}
