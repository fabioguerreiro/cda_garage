/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.garage;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author faguerreiro
 */
public class Vehicules {
    private final SimpleIntegerProperty id;
    private final SimpleStringProperty modele;
    private final SimpleStringProperty marque;
    private final SimpleStringProperty immatriculation;

    public Vehicules(Integer id, String marque, String modele, String immatriculation) {
        super();
        this.id = new SimpleIntegerProperty(id);
        this.modele = new SimpleStringProperty(modele);
        this.marque = new SimpleStringProperty(marque);
        this.immatriculation = new SimpleStringProperty(immatriculation);
    }

    public Integer getId() {
        return id.get();
    }

    public String getModele() {
        return modele.get();
    }
    
    public String getMarque() {
        return marque.get();
    }

    public String getImmatriculation() {
        return immatriculation.get();
    }
}
