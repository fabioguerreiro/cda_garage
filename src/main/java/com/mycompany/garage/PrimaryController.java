package com.mycompany.garage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class PrimaryController implements Initializable{

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
    
    @FXML private TableView<Vehicules> vehicule;
    @FXML private TableColumn<Vehicules, Integer> id;
    @FXML private TableColumn<Vehicules, String> marque;
    @FXML private TableColumn<Vehicules, String> modele;
    @FXML private TableColumn<Vehicules, String> immatriculation;
    
    public ObservableList<Vehicules> v = FXCollections.observableArrayList(
            new Vehicules (1,"Renault", "Megane", "AC 254 VP")
            );

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        id.setCellValueFactory(new PropertyValueFactory<Vehicules, Integer>("Id"));
        marque.setCellValueFactory(new PropertyValueFactory<Vehicules, String>("Marque"));
        modele.setCellValueFactory(new PropertyValueFactory<Vehicules, String>("Modele"));
        immatriculation.setCellValueFactory(new PropertyValueFactory<Vehicules, String>("Immatriculation"));
        vehicule.setItems(v);
    }
}
